import React from "react";
import MenuOffer from "../components/MenuOffer";
import Header from "../components/Header";

const Catering = () => {
  return (
    <>
      <Header defaultColor={true}/>
      <MenuOffer />
    </>
  );
};

export default Catering;
