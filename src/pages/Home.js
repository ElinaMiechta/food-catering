import Calculator from "../components/CalculatorSection";
import HeroSeaction from "../components/HeroSeaction";
import ParallaxSection from "../components/ParralaxSection";
import Section from "../components/Section";
import Header from '../components/Header'; 

const Home = () => {
  return (
    <>
        <Header/>
      <HeroSeaction />
      <Section
        header="Welcome to FoodMood!"
        headerTxt="We inspire people to lead healthy lifestyle!"
        textTop={253}
        imgTop={127}
        blok1ImgSrc="https://images.unsplash.com/photo-1482049016688-2d3e1b311543?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=653&q=80"
        block2ImgSrc="https://images.unsplash.com/photo-1484723091739-30a097e8f929?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80"
      />
      <ParallaxSection
        header="We save your time by serving best food"
        text="You choose When & Were, we choose best ingridients"
        isButton={true}
        imgUrl={
          "https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
        }
        topSpace={160}
      />
      <Calculator />
      <div className="col-dark">
        <Section
          header="Feel free to contact Us by Social Media!"
          headerTxt="Professional team, on-time delivery and many more are waiting for You!"
          textTop={2260}
          imgTop={2107}
          blok1ImgSrc="https://images.unsplash.com/photo-1587248720327-8eb72564be1e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
          block2ImgSrc="https://images.unsplash.com/photo-1579293907257-cc0f8a01f368?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
        />
      </div>
      <ParallaxSection
        header="Have you checked your calories norma?"
        text="Go to menu propositions"
        isButton={true}
        imgUrl="https://images.unsplash.com/photo-1499028344343-cd173ffc68a9?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
        topSpace={0}
      />
    </>
  );
};

export default Home;
