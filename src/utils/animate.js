export const animate = (htmlElement, coordinateTop, className) => {
  window.addEventListener("scroll", function () {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    // console.log(top);
    if (top >= coordinateTop) {
      htmlElement.classList.add(className);
      if (window.screen.width < 990) {
        htmlElement.classList.remove(className);
      }
    }
  });
};
