import {
  Radio,
  FormControlLabel,
  Input,
  RadioGroup,
  Button,
} from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import React, { useState, useEffect } from "react";

const Calculator = () => {
  const [res, setRes] = useState(0);
  const [userParams, setUserParams] = useState({
    age: 0,
    height: 0,
    weight: 0,
    sex: null,
  });
  const [checked, setChecked] = useState(false);

  const handleGroupChange = (e) => {
    setUserParams({
      ...userParams,
      sex: e.target.value,
    });
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    switch (name) {
      case "age":
        setUserParams({
          ...userParams,
          age: parseInt(value),
        });
        break;
      case "weight":
        setUserParams({
          ...userParams,
          weight: parseInt(value),
        });
        break;
      case "height":
        setUserParams({
          ...userParams,
          height: parseInt(value),
        });
        break;
    }
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    let ccal;
    if (
      userParams.age <= 0 ||
      userParams.weight <= 0 ||
      userParams.height <= 0 ||
      !userParams.sex
    ) {
      alert("Some of the required data is missing.");
      return;
    } else if (userParams.sex === "female") {
      ccal =
        655.09 +
        9.56 * userParams.weight +
        1.84 * userParams.height -
        4.67 * userParams.age;
    } else {
      ccal =
        66.47 +
        13.75 * userParams.weight +
        5 * userParams.height -
        6.75 * userParams.age;
    }
    setRes(Math.floor(ccal));
    setUserParams({
      age: 0,
      height: 0,
      weight: 0,
      sex: null,
    });
  };

  return (
    <div className="calculator">
      <div className="calculator__upper">
        <h2>Calculator</h2>
        <span>Check Your daily norma and choose one, that fit the best.</span>
        <div className="calculator__result">
          <p>Result:  {res} ccal/day</p>
        </div>
      </div>
      <div className="calculator__bottom">
        <div className="calculator__left">
      <Input
            placeholder="Age"
            type="text"
            name="age"
            color="secondary"
            onChange={(e) => handleChange(e)}
            value={!userParams.age  ? "" : userParams.age}
          />
          <Input
            placeholder="Height /cm"
            type="text"
            name="height"
            color="secondary"
            onChange={(e) => handleChange(e)}
            value={!userParams.height ? "" : userParams.height}
          />
          </div>
          <div className="calculator__right">
          <Input
            placeholder="Weight /kg"
            type="text"
            name="weight"
            color="secondary"
            onChange={(e) => handleChange(e)}
            value={!userParams.weight ? "" : userParams.weight}
          />
          <div className="calculator__right--group">
            <RadioGroup
              aria-label="gender"
              name="gender1"
              value={userParams.sex}
              onChange={(e) => handleGroupChange(e)}>
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
              />
              <FormControlLabel value="male" control={<Radio />} label="Male" />
            </RadioGroup>
            </div>
          </div>
          <div className="calculator__submit">
      
          <Button
            className="calculator--outer__form--btn"
            variant="outlined"
            type="button"
            onClick={(e) => handleSubmit(e)}>
            Submit
          </Button>
          </div>
      </div>
      
     

    </div>
  );
};

export default Calculator;
