import { useRef, useEffect } from "react";

import { Button } from "@material-ui/core";
import { animate } from "../../utils/animate";
import {Link} from 'react-router-dom';

const ParallaxSection = ({ header, text, isButton, imgUrl, topSpace }) => {
  const txtRef = useRef(null);
  const headerRef = useRef(null);
  useEffect(() => {
    animate(txtRef.current, 1050, "leftShowUp");
    animate(headerRef.current, 1010, "textAppear");
  });

  const image = {
    backgroundImage: `url(${imgUrl})`,
    marginTop: `${topSpace}px`
  };
  return (
    <div className="parallax" style={image}>
      <div className="parallax--bgContainer"></div>
      <div className="parallax--fgContainer">
        <h1 ref={headerRef}>{header}</h1>
        <h2 ref={txtRef}>{text}</h2>
        {isButton && (
          <Button className="parallax--fgContainer--btn" variant="outlined">
            <Link to="/menu">Check Menu</Link>
          </Button>
        )}
      </div>
    </div>
  );
};

export default ParallaxSection;
