import { useEffect, useRef, useState } from "react";

import { navData } from "../data/navData";
import { Button } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import Menu from "./Menu";


const Header = ({defaultColor}) => {
  const menuRef = useRef(null);
  const headerRef = useRef(null);
  const [visible, setVisible] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [color, setColor] = useState(defaultColor ? '#1b1b1b' : '');

  const changeHeaderColor = () => {
    if(defaultColor) {
      return;
    }
    let color;
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    return top >= 600 ?   setColor('#1b1b1b') : setColor('transparent');
  };

  useEffect(() => {
    window.addEventListener('scroll', changeHeaderColor);
    return () => window.removeEventListener('scroll', changeHeaderColor);
    });

  const showBtn = () => {
    if (window.screen.width <= 960) {
      setVisible(true);
    } else {
      setVisible(false);
    }
  };

  useEffect(() => {
    showBtn();
  }, []);

  useEffect(() => {
    if (clicked) {
      menuRef.current.classList.add("mobile-show");
    } else {
      menuRef.current.classList.remove("mobile-show");
    }
  }, [clicked]);

  window.addEventListener("resize", showBtn);

  return (
    <div className="header" ref={headerRef} style={{backgroundColor: color}}>
      <div className={`header__logo ${clicked ? 'hide' : ''}`}>FoodMood</div>
      <div className="header__items" ref={menuRef}>
        {clicked && <Menu data={navData} clicked={clicked} />}
        {!visible ? (
          <Menu data={navData} />
        ) : (
          <MenuIcon
            className="header__items--mobile"
            onClick={() => setClicked(!clicked)}
          />
        )}
      </div>

      {!visible && (
        <div className="header__signin show">
          <Button className="header__signin--btn" variant="outlined">
            Sign In
          </Button>
        </div>
      )}
    </div>
  );
};

export default Header;
