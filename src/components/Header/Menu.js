import {Link} from 'react-router-dom';

const Menu = ({data, clicked}) => {
      return (
            <ul className="header__items--list" style={clicked && {flexDirection: 'column', fontSize: '2rem'}}>
            {data.map((el, key) => (
              <li className="items--list__navElement" key={key + el.title} style={clicked && {marginTop: '1rem'}}>
                <Link to={el.href}>{el.title}</Link>
              </li>
            ))}

            {clicked && <li className="items--list__navElement" style={clicked && {marginTop: '1rem'}}>Sign In</li>}
          </ul>
      )

}

export default Menu