import React from "react";

const MenuCard = ({ img, title, ccal, onClick }) => {
  return (
    <div className="card">
      <div className="card--img">
        <img src={img} alt="diet!" />
      </div>
      <div className="card--info">
        <div className="card--info__content">
          <h3 className="card__title">{title}</h3>
          <p className="card__ccal">{ccal}</p>
        </div>
        <div className="card--info__action">
          <span onClick={onClick}>See more! </span>
        </div>
      </div>
    </div>
  );
};

export default MenuCard;
