import { useState, useEffect } from "react";
import MenuCard from "./MenuCard";
import { dietsDB } from "../data/dietsDB";
import MenuInfo from "./MenuInfo";

const MenuOffer = () => {
  const [index, setIndex] = useState(
    localStorage.getItem("id") ? localStorage.getItem("id") : null
  );

  useEffect(() => {
    localStorage.setItem("id", index);
  }, [index]);

  return (
    <div className="offerContainer">
      <div className="offerContainer__title">
        <h1>Menu</h1>
        <h2>FoodMood</h2>
        <span className="divider"></span>
      </div>
      <div className="offerContainer__menuSection">
        {dietsDB.map((diet, index) => (
          <MenuCard
            key={index}
            img={diet.image}
            title={diet.title}
            ccal={diet.ccal}
            onClick={() => setIndex(diet.id)}
          />
        ))}
      </div>

      <div className="offerContainer__keyInfo">
        {index && <MenuInfo id={index} />}
      </div>
    </div>
  );
};

export default MenuOffer;
