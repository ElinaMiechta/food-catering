import { useState, useEffect } from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from "@material-ui/core";
import InfoRow from "./InfoRow";
import { dietsDB } from "../data/dietsDB";
import { dietsMenu } from "../data/dietsDB";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

const MenuInfo = ({ title, id }) => {
  const [progress, setProgress] = useState(0);
  const [menuOffer, setMenuOffer] = useState("");
  const [actualDiet, setActualDiet] = useState(dietsMenu[0]);
  const [actualDay, setActualDay] = useState("mon");

  useEffect(() => {
    manageProgressbar(actualDay);
  }, [actualDay]);

  useEffect(() => {
    let actual = dietsDB.filter((diet) => {
      return diet.id == id;
    });
    setMenuOffer(actual[0]);
  }, [id]);

  useEffect(() => {
    dietsMenu.forEach((d) => {
      if (d.id === id) {
        setActualDiet(d);
      }
    });
  }, [id]);

  const classes = useStyles();

  useEffect(() => {
    if (progress >= 100) {
      setProgress(0);
    }
  }, [progress]);

  const convertToDayFromDB = (day) => {
    switch (day) {
      case "mon":
        return actualDiet.mon;
      case "tue":
        return actualDiet.tue;
      case "wed":
        return actualDiet.wed;
      case "tuer":
        return actualDiet.tuer;
      case "fri":
        return actualDiet.fri;
      case "sat":
        return actualDiet.sat;
      case "sun":
        return actualDiet.sun;
      default:
        alert("No day!");
    }
  };

  const manageProgressbar = (day) => {
    switch (day) {
      case "mon":
        setProgress(15);
        break;
      case "tue":
        setProgress(30);
        break;
      case "wed":
        setProgress(45);
        break;
      case "tuer":
        setProgress(60);
        break;
      case "fri":
        setProgress(75);
        break;
      case "sat":
        setProgress(90);
        break;
      case "sun":
        setProgress(100);
        break;
      default:
        alert("No day!");
    }
  };

  const triggerActiveDayOfTheWeek = (e) => {
    setActualDay(e.target.className.split(" ")[0]);
    const items = document
      .querySelector(".info__week--col")
      .querySelectorAll("span");
    items.forEach((el) => {
      return el !== e.target
        ? el.classList.remove("colored")
        : el.classList.add("colored");
    });
  };

  return (
    <div className="info">
      <div className="info__title">
        <h2>{title}</h2>
      </div>
      <div className="info__week">
        <div className="info__week--col">
          <span className="mon" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Monday
          </span>
          <span className="tue" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Thuesday
          </span>
          <span className="wed" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Wendsday
          </span>
          <span className="tuer" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Thuersday
          </span>
          <span className="fri" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Friday
          </span>
          <span className="sat" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Saturday
          </span>
          <span className="sun" onClick={(e) => triggerActiveDayOfTheWeek(e)}>
            Sunday
          </span>
        </div>
        <div className={classes.root}>
          <LinearProgress variant="determinate" value={progress} />
        </div>
      </div>

      <div className="info__card">
        <div className="info__card--left">
          <h2>{menuOffer.title}</h2>

          <InfoRow
            title="Breakfast"
            iconClass="fas fa-bread-slice"
            text={convertToDayFromDB(actualDay)[0].breakfast}
          />
          <InfoRow
            title="Morning Snack"
            iconClass="fas fa-cookie"
            text={convertToDayFromDB(actualDay)[0].snack1}
          />
          <InfoRow
            title="Lunch"
            iconClass="fas fa-utensils"
            text={convertToDayFromDB(actualDay)[0].lunch}
          />
          <InfoRow
            title="Evening Snack"
            iconClass="fas fa-carrot"
            text={convertToDayFromDB(actualDay)[0].snack2}
          />
          <InfoRow
            title="Dinner"
            iconClass="fas fa-seedlingfas fa-seedling"
            text={convertToDayFromDB(actualDay)[0].dinner}
          />
        </div>
        <div className="info__card--right">
          <span>Alergens</span>
          <span>
            You may find following alergens in this diet: <br />
            <strong>{menuOffer.alergens}</strong>
          </span>
        </div>
      </div>
    </div>
  );
};

export default MenuInfo;
