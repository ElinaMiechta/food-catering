import React from "react";

const InfoRow = ({ title, iconClass, text }) => {
  return (
    <div className="info__row">
      <div className="info__row--icon">
        <i className={iconClass}></i>
      </div>
      <div className="info__row--text">
        <p className="info__row--title">{title}</p>
        <p className="info__row--info">{text}</p>
      </div>
    </div>
  );
};

export default InfoRow;
