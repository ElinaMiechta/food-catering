

const Slider = ({ data }) => {
  return (
    <>
      {data.map((el, index) => (
        <div className="banner" key={index} style={{ animation: el.animation }}>
          <img className="banner__img" src={el.img} alt="FoodMood offer :)" />
          <div
            className="banner__text-box text-box1"
            style={{ animationDelay: el.delay }}>
            <h1>{el.title}</h1>
            <span></span>
            <p>{el.descr}</p>
          </div>
        </div>
      ))}
    </>
  );
};

export default Slider;
