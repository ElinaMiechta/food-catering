import {useEffect, useRef} from 'react'
import {animate} from '../../utils/animate'

const Section = (
      {
            header,
            headerTxt,
            textTop,
            imgTop,
            blok1ImgSrc,
            block2ImgSrc
            }
) => {
      const textRef = useRef(null);
      const imgRef = useRef(null);

      useEffect(() => {
     animate(textRef.current, textTop, 'leftShowUp' )
     animate(imgRef.current, imgTop, 'rightShowUp' )
      });


      return (
            <div className="sectionContainer">
                  <div className="sectionContainer__left" ref={textRef}>
                        <h2>{header}</h2>
                        <h3>{ headerTxt}</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                  </div>
                  <div className="sectionContainer__right" ref={imgRef}>
                        <div className="sectionContainer__right--block1">
                              <img src={blok1ImgSrc} alt="healty diet!"/>
                        </div>
                        <div className="sectionContainer__right--block2">
                              <img src={block2ImgSrc} alt="healty diet!"/>
                        </div>
                  </div>
                  
            </div>
      )
}

export default Section
