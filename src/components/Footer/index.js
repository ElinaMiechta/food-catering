import { Button } from '@material-ui/core'
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";

const Footer = () => {
      return (
            <div className="footer">
                  <div className="footer--main">
                   <div className="footer__adress">
                         <h4>Adress</h4>
                         <p>133A Oxford blv., Chiara, Chiara-Field</p>
                         <p>133A Oxford blv., Chiara, Chiara-Field</p>
                         <p>133A Oxford blv., Chiara, Chiara-Field</p>
                   </div>
                   <div className="footer__about">
                         <h4>FoodMood</h4>
                         <span className="divider"></span>
                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                   </div>
                  <div className="footer__contact">
                        <h4>Contact</h4>
                        <p>+1 0000000</p>
                        <p>+3 499990</p>
                        <div className="contact--smLinks">
                        <InstagramIcon className="smLinks__icon" />
                        <FacebookIcon className="smLinks__icon" />
                        <YouTubeIcon className="smLinks__icon" />
                        </div>
                  </div>
                  </div>
                  <div className="footer--links">
                        <Button className="footer__links--btn">About</Button>
                        <Button className="footer__links--btn">Blog</Button>
                        <Button className="footer__links--btn">Gallery</Button>
                  </div>
            </div>
      )
}

export default Footer
