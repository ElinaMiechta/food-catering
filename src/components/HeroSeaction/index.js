import React from "react";
import { sliderData } from "../data/sliderData";
import Slider from "../Slider";

const HeroSection = () => {
  return (
    <div className="section">
      <Slider data={sliderData} />
    </div>
  );
};

export default HeroSection;
