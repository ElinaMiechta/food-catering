export const dietsDB = [
  {
    id: "1",
    title: "Sport Diet",
    image: "/images/food1_edited.png",
    descr:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
    alergens: "Milk, Nuts",
    ccal: "1500,1800,2000,2500",
  },
  {
    id: "2",
    title: "Standart Diet",
    image: "/images/food1_edited.png",
    descr:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
    alergens: "Nuts, Chocolate, Lactose, Gluten, Eggs",
    ccal: "1000,1200,1500,1800,2000,2500",
  },
  {
    id: "3",
    title: "Vege Diet",
    image: "/images/food1_edited.png",
    descr:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
    alergens: "Nuts,Gluten, Citrus",
    ccal: "1200,1800,2000",
  },
];

export const dietsMenu = [
  {
    id: "1",
    mon: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tue: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    wed: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tuer: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    fri: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    sat: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    sun: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
  },
  {
    id: "2",
    mon: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tue: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    wed: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tuer: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    fri: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    sat: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    sun: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
  },
  {
    id: "3",
    mon: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tue: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    wed: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    tuer: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    fri: [
      {
        breakfast:
          "Sandwich with French pate with onion and currant jam with vegetables, Protein shake *1,7,12",
        snack1: "Semolina porridge with roasted plum and maple syrup *1,7",
        lunch:
          "Arrabbiata pasta with turkey and mozzarella, Brussels sprouts - lentil cream soup *1,7,9",
        snack2:
          "2500-3000-4000 kcal: Cheesecake with cherry mousse on the bottom of oatmeal cookies *1,3,7 / 4000kcal: Blueberry smoothie",
        dinner: "Broccoli tart with cheese, walnuts and garlic sauce *1,3,7,8",
      },
    ],
    sat: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
    sun: [
      {
        breakfast:
          "Stracciatella porridge with mango, Vanilla protein shake *1,7",
        snack1:
          "Cottage cheese with zucchini and pumpkin seeds with bread *1,7",
        lunch:
          "Pork tenderloin in mushroom sauce with potatoes and Brussels sprouts, Corn and fennel cream soup *7",
        snack2:
          "2500-3000-4000 kcal: Cookies with coconut flakes *1,7,12 / 4000kcal: Fruit smoothie",
        dinner:
          "Salad with tuna, long-maturing cheese and vegetables *3,4,7,10",
      },
    ],
  },
];
