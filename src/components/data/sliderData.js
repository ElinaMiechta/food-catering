
export const sliderData = [
      {
            "title": "Find out our lates promotions!",
            "descr": "Discover catering menu and choose the one that covers your need the most. We offer diets for training, feet, vegan, carbonate, detox and more!",
            "img": "https://images.unsplash.com/photo-1500291161618-747dee2ab16c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80",
            "animation": "slide1 12s linear infinite",
            "delay": "0s"
      },
      {
            "title": "Get a Diet Menu which fits you!",
            "descr": "Discover catering menu and choose the one that covers your need the most. We offer diets for training, feet, vegan, carbonate, detox and more!",
            "img": "https://images.unsplash.com/photo-1473340325660-0f786fd1571c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80",
            "animation": "slide2 12s linear infinite",
            "delay": "3s"
      },
      {
            "title": "Contact us to get professional advices!",
            "descr": "Discover catering menu and choose the one that covers your need the most. We offer diets for training, feet, vegan, carbonate, detox and more!",
            "img": "https://images.unsplash.com/photo-1458253756247-1e4ed949191b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80",
            "animation": "slide3 12s linear infinite",
            "delay": "6s"
      },
      {
            "title": "Book 1 trial day for special price!",
            "descr": "Discover catering menu and choose the one that covers your need the most. We offer diets for training, feet, vegan, carbonate, detox and more!",
            "img": "https://images.unsplash.com/photo-1548435297-6fcc2b3def2f?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1112&q=80",
            "animation": "slide4 12s linear infinite",
            "delay": "9s"
      }
];
