import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Footer from './components/Footer';

import Header from "./components/Header";
import Home from './pages/Home';
import Catering from './pages/Catering'


function App() {
  return (
 
    <div className="container">  
     <Router>
     {/* <Header/> */}
       <Switch>
         <Route exact path="/" component={Home}/>
         <Route  path="/menu" component={Catering}/>
         </Switch>

     </Router>
     <Footer/>
    </div>
  );
}

export default App;
